{ pkgs, config, ... }:
{
  nix.extraOptions =
    let
      nix-plugins = pkgs.nix-plugins.override {
        nix = config.nix.package;
      };
    in
    ''
      plugin-files = ${nix-plugins}/lib/nix/plugins/libnix-extra-builtins.so
      extra-builtins-file = /home/slabity/Nix/plugins/extra-builtins.nix
    '';

  imports = [
    ./lib
    ./hardware.nix
    ./passwd.nix
    ./nix.nix
    ./desktop.nix
    ./printing.nix
    ./fonts.nix
  ];
}
