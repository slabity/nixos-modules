{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.foxos;
  desktopCfg = cfg.desktop;
in
{
  options.foxos.desktop.enable = mkEnableOption "Enable desktop support";

  config = mkIf desktopCfg.enable {

    boot.kernelPackages = pkgs.linuxPackages_latest;

    boot.loader.grub.enable = true;
    boot.loader.grub.useOSProber = true;
    boot.loader.timeout = 3;

    virtualisation.libvirtd.enable = true;

    services.dbus.socketActivated = true;
    services.dbus.packages = with pkgs; [ gnome3.dconf ];

    services.avahi.enable = true;
    services.mpd.enable = true;
    hardware.pulseaudio = mkIf cfg.hardware.audio.enable {
      enable = true;
      package = pkgs.pulseaudioFull;
      support32Bit = cfg.hardware.cpu.support32Bit;
    };

    networking.networkmanager.enable = true;
    networking.networkmanager.unmanaged = [ "interface-name:ve-*" ];

    services.xserver = {
      enable = true;

      layout = "us";
      libinput = {
        enable = true;
        accelProfile = "flat";
        tapping = false;
        tappingDragLock = false;
      };

      displayManager.job.logToJournal = true;
      displayManager.lightdm.enable = true;
      displayManager.lightdm.autoLogin = {
        enable = true;
        user = cfg.mainUser.name;
      };
      displayManager.lightdm.greeter.enable = false;

      desktopManager.xterm.enable = false;

      videoDrivers = [ "amdgpu" "modesetting" ];
      useGlamor = true;

      desktopManager.session = [
        {
          name = "custom";
          start = "";
        }
      ];

      desktopManager.default = "custom";
    };

    services.udev.extraRules = ''
        # Enable BFQ IO Scheduling algorithm
        ACTION=="add|change", KERNEL=="[sv]d[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
        ACTION=="add|change", KERNEL=="[sv]d[a-z]", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="bfq"
        ACTION=="add|change", KERNEL=="nvme[0-9]n1", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="bfq"

        # Ultimate Hacking Keyboard rules
        # These are the udev rules for accessing the USB interfaces of the UHK as non-root users.
        # Copy this file to /etc/udev/rules.d and physically reconnect the UHK afterwards.
        SUBSYSTEM=="input", GROUP="input", MODE="0666"
        SUBSYSTEMS=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="612[0-7]", MODE:="0666", GROUP="plugdev"
        KERNEL=="hidraw*", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="612[0-7]", MODE="0666", GROUP="plugdev"

        # DualShock 3 controller, Bluetooth
        KERNEL=="hidraw*", KERNELS=="*054C:0268*", MODE="0660", TAG+="uaccess"
        # DualShock 3 controller, USB
        KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="0268", MODE="0660", TAG+="uaccess
    '';

    environment.systemPackages = with pkgs; [
      nix-index
      nix-prefetch-git

      unzip zip unrar

      pciutils usbutils atop
      pstree

      file bc psmisc

      git manpages
    ];
  };
}
