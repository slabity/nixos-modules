{ config, lib, pkgs, ... }:
with lib;
{
  fonts.enableDefaultFonts = true;
  fonts.fonts = with pkgs; [
    corefonts source-code-pro source-sans-pro source-serif-pro
    font-awesome-ttf terminus_font powerline-fonts google-fonts inconsolata noto-fonts
    noto-fonts-cjk unifont ubuntu_font_family nerdfonts
  ];
  fonts.fontconfig = {
    enable = true;
    defaultFonts.monospace = [
      "Terminess Powerline"
      "TerminessTTF Nerd Font Mono"
    ];
    defaultFonts.emoji = [ "Noto Color Emoji" ];
    hinting.enable = true;
    antialias = true;
    penultimate.enable = false;
  };
}
