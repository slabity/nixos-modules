{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.foxos;
  userCfg = cfg.mainUser;

  #homeMgrPkg = fetchTarball "https://github.com/rycee/home-manager/archive/master.tar.gz";
in
{
  imports = [
  #  "${homeMgrPkg}/nixos"
  ];

  options.foxos.mainUser = {
    enable = mkEnableOption "Main (non-root) user";
    name = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "Main user name";
    };
  };

  config = mkIf userCfg.enable {
    users = {
      defaultUserShell = pkgs.zsh;
      mutableUsers = false;

      users.${userCfg.name} = {
        uid = 1000;
        isNormalUser = true;

        extraGroups = [
          "wheel"
          "networkmanager"
          "docker"
          "libvirtd"
          "dialout"
          "adbusers"
        ];

        passwordFile = "${/home/slabity/Nix/passwd}";
        #hashedPassword = (builtins.extraBuiltins.pass "users/${userCfg.name}" "sha512");
      };
    };

    assertions = [
      {
        assertion = userCfg.name != null;
        message = "Required variable 'userCfg.name' not set";
      }
    ];
  };
}
