{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.foxos;

  cpuCfg = cfg.hardware.cpu;
  totalThreads = cpuCfg.sockets * cpuCfg.cores * cpuCfg.threads;

  isDesktop = cfg.desktop.enable;

  responsiveNiceLevel = 5;

  #nix-plugins = pkgs.nix-plugins.override {
  #  nix = config.nix.package;
  #};

  #plugins = fetchGit {
  #  url = "https://gitlab.com/slabity/nix-plugins.git";
  #};
in
{
  config = {
    nix.daemonNiceLevel = mkIf isDesktop responsiveNiceLevel;
    nix.daemonIONiceLevel = mkIf isDesktop responsiveNiceLevel;

    nix.maxJobs = totalThreads;
    nix.buildCores = totalThreads;

    nix.readOnlyStore = true;
    nix.autoOptimiseStore = true;

    nix.trustedUsers = [ "root" "@wheel" ];

    #nix.extraOptions = ''
    #  plugin-files = ${nix-plugins}/lib/nix/plugins/libnix-extra-builtins.so
    #'';
  };
}
